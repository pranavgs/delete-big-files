#!/bin/bash
#!/usr/bin/env bash


IFS=$'\n';

read -p 'Please enter the number of files/directories to be displayed : ' usrno

#array for displaying size and directory location

if [ -z "$usrno" ]
then
      echo "Empty Input"
      exit
else
      echo $usrno
      declare -a resarray
      resarray=($(sudo du -h /home/pranavgs/Documents/Test2 | sort -n -r | head -n $usrno | awk '{print $1,$2}'))
      COUNT=`expr ${#resarray[@]} - 1`
      #echo "${resarray[*]}"
fi

#For displaying the index number along with the array elements

for I in ${!resarray[*]}; do
  echo $I: ${resarray[$I]}
done

#array for deleting the user selected directory (Only the directory location is there)

declare -a resarray1
resarray1=($(sudo du -h /home/pranavgs/Documents/Test2 | sort -n -r | head -n 20 | awk '{print $2}'))
COUNT=`expr ${#resarray1[@]} - 1`

#Reading the values into an array for Multiple deletion

read -p "Do you want to delete  (y/n)?" CONT
if [ "$CONT" = "y" ]; then
  echo 'Please enter the number of the directory to be deleted : '
  while read line
  do
      [ "$line" == "" ] && break
      user_array=("${user_array[@]}" $line)
  done

  if [ -z "$user_array" ]
  then
    echo "Empty input"
    exit
  else
    for values in ${user_array[@]}
    do
      sudo rm -rf ${resarray1[$values]}
      echo The ${resarray1[$values]} has been deleted
  done
fi
else
  echo "Bye";
  exit
fi
